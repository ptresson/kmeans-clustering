import os
import torch
from torch import nn
from collections import OrderedDict
import numpy as np
import pandas as pd

import torchvision
from torchvision import models as torchvision_models
from torchvision.models.detection import MaskRCNN
from torchvision.models.detection.backbone_utils import resnet_fpn_backbone
from torchvision.models.detection.rpn import AnchorGenerator
import torchvision.transforms as T
from torchvision import datasets, transforms
from PIL import Image
import random

from sklearn.cluster import KMeans

from osgeo import gdal

import vision_transformer as vits


pd.set_option('display.max_colwidth', None)

###################################################################
load pretrained backbone
x = torch.rand(1,3,256,256)
backbone  = torchvision_models.__dict__["resnet50"]()
backbone.fc = nn.Identity()
backbone.out_channels = 2048

checkpoint = torch.load('/home/ptresson/DINO/runs/2022png_resnet/checkpoint0080.pth')
# checkpoint = torch.load('/home/ptresson/DINO/dino/output/checkpoint.pth')
# checkpoint = torch.load('/home/ptresson/DINO/runs/1959_resnet/checkpoint.pth')

d = checkpoint['teacher']
# remove head layers and rename backbone layers
d2 = OrderedDict([(k[16:], v) for k, v in d.items() if ('module.backbone' in k)])
backbone.load_state_dict(d2)
##################################################################
# load images
data_dir = '/home/ptresson/congo/crop2022png/'
# data_dir = '/home/ptresson/congo/crop1959/'
transform = T.ToTensor()
dataset = datasets.ImageFolder(data_dir, transform=transform)

dataloader = torch.utils.data.DataLoader(
    dataset,
    batch_size=1,
    num_workers=0,
    shuffle=False,
)

# Get features for all images
features_list = []
names_list = []
i = 0
for image, _ in dataloader :

    y = backbone(image)
    features_list.append(y.squeeze().cpu().detach().numpy())
    i +=1
    print(f"{i}/{len(dataloader)}",end="\r")


##################################################################
# fit kmeans on all features
X = np.array(features_list)
kmeans = KMeans(n_clusters=4, random_state=0).fit(X.astype('double'))

# print(kmeans.predict(np.random.rand(1,2048)))
# print(kmeans.cluster_centers_)

# get image coordinates on original file from image names
names_list = [x for (x,_) in dataset.imgs]
coord_list = []
for name in sorted(names_list) :
    without_ext = name.split('.')[0]
    x = without_ext.split('_')[-2]
    y = without_ext.split('_')[-1]
    coord_list.append((x,y))

##################################################################
# write features to csv
f = open("features_resnet_congo_2022png.csv", "w")
for i in range(len(features_list)):
    f.write(str(coord_list[i]))
    f.write(";")
    for feature in features_list[i]:
        f.write(str(feature))
        f.write(",")
    f.write("\n")
f.close()

##################################################################
## Inference to raster

# file_to_modif = '/home/ptresson/congo/1959_modif.tif'
file_to_modif = '/home/ptresson/congo/2022_modif.tif'
image = gdal.Open(file_to_modif,gdal.GA_Update)

print(image.RasterCount, image.RasterXSize, image.RasterYSize)

rband = image.GetRasterBand(1)
r = rband.ReadAsArray()
r = (r != 0).astype(np.uint8) * 0
print(r.shape)

for i in range(len(names_list)):
    (x,y) = coord_list[i]
    x = int(x)
    y = int(y)
    r[y:y+256,x:x+256] = int(kmeans.predict([features_list[i]])[0])
    print(kmeans.predict([features_list[i]])[0])
    # r[x:x+256,y:y+256] = int(kmeans.predict([features_list[i]])[0])*10 + 100

# print(np.unique(r))
print(np.unique(r))
image.GetRasterBand(1).WriteArray(r)   # write r-band to the raster
# image.GetRasterBand(3).WriteArray(b)   # write b-band to the raster
