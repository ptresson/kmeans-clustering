import time
import re
import os
import torch
from torch import nn
from collections import OrderedDict
import numpy as np
import pandas as pd

import torchvision
from torchvision import models as torchvision_models
import torchvision.transforms as T
from torchvision import datasets, transforms
from PIL import Image
import random

from sklearn.cluster import KMeans

from osgeo import gdal

pd.set_option('display.max_colwidth', None)


###################################################################
# df = pd.read_csv("features_resnet_congo_2022png.csv",sep=";")
df = pd.read_csv("features_resnet_congo_2022.csv",sep=";")
features_list = df[df.columns[1]].to_list()
features_list = [list(x.split(',')) for x in features_list]
features_list = [x[:-1] for x in features_list]


coord_list = df[df.columns[0]].to_list()
# coord_list = [tuple(map(int, re.sub('[^0-9]','', x.split(",")))) for x in coord_list]
coord_list = [tuple(s.replace("(","").replace(")","").replace("'","").split(",")) for s in coord_list]
print(coord_list)
##################################################################
# fit kmeans on all features
X = np.array(features_list)
kmeans = KMeans(n_clusters=4, random_state=0).fit(X.astype('double'))
print("kmeans ok \n")

##################################################################
## Inference to raster

# file_to_modif = '/home/ptresson/congo/1959_modif.tif'
file_to_modif = '/home/ptresson/congo/2022_modif.tif'
image = gdal.Open(file_to_modif,gdal.GA_Update)

print(image.RasterCount, image.RasterXSize, image.RasterYSize)

rband = image.GetRasterBand(1)
r = rband.ReadAsArray()
r = (r != 0).astype(np.uint8) * 0
print(r.shape)

for i in range(len(coord_list)):
    (x,y) = coord_list[i]
    x = int(x)
    y = int(y)
    r[y:y+256,x:x+256] = int(kmeans.predict([features_list[i]])[0])
    print(kmeans.predict([features_list[i]])[0])
    # r[x:x+256,y:y+256] = int(kmeans.predict([features_list[i]])[0])*10 + 100

print(np.unique(r))
image.GetRasterBand(1).WriteArray(r)   # write r-band to the raster
